import React from 'react';
import './App.css';
import Main from "./components/main/main";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 id="title">Carlos Front</h1>
      </header>
      <Main/>
    </div>
  );
}

export default App;
