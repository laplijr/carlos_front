import React from 'react';
import { shallow } from './enzyme';
import App from "./App";

describe('App', () => {
  it('should have a header', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('header')).toHaveLength(1);
    expect(wrapper.find('h1#title').text()).toEqual('Carlos Front');
  })
});
