import React, {useState} from 'react';
import axios from 'axios';
import Files from 'react-files';

export const Main = () => {

  const [selectedFile, setSelectedFile] = useState(null);
  const onChangeHandler = e => {
    if (e.length === 1) {
      setSelectedFile(e[0])
    }
  };

  const handleMediaError = () => {
  };

  const onClickHandler = () => {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    const data = new FormData();
    data.append('file', selectedFile);
    axios.post(process.env.REACT_APP_API_URL + "/datas", data, {headers})
      .then(() => alert("Le fichier a bien été traité"))
      .catch(() => alert('Votre fichier n\'a pas pu être traité. Une erreur est survenue'))
  };

  return (
    <>
      <div id={"upload"}>
        <span>{selectedFile !== null ? selectedFile.name : null}</span>
        <Files
          accepts={['.csv']}
          multiple={false}
          onChange={onChangeHandler}
          onError={handleMediaError}
        >
          <button id="csv" onClick={e => e.preventDefault()}>Sélectionner un fichier csv</button>
        </Files>
      </div>
      <button type="button" onClick={onClickHandler}>Envoyer</button>
    </>
  )
};

export default Main;
