describe('App E2E', () => {
    it('should have a button with a defined label', () => {
        cy.visit('http://localhost:3000');
        cy.get('div#upload').within(() =>{
            cy.get('button#csv').should('have.text', 'Sélectionner un fichier csv');
        })
    });
});
